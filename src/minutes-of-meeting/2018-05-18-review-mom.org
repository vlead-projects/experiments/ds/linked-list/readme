#+title: Minutes of Meeting 2018-05-18 
#+AUTHOR: Sumaid and Swapnil
#+DATE: [2018-05-18 Fri]
#+SETUPFILE: ../../org-templates/level-2.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
 Here are the minutes of meeting held on 18th may:

** Duration
 From 11:15 to 11:30 

** Attendees
 Proff Venkatesh Chopella
 Proff Priya
 Sumaid Syed
 Swapnil Pakhare

** Purpose
 - To discuss the experiment content structure developed so far.
   
** Takeaways
 - Got a better understanding on how to go about
   implementing our experiment and what exactly each unit
   will hold.
 - Got an overall image to show linked list representations.

** Action
 - Will work on understanding the structure better now and
   plan further course of action.
