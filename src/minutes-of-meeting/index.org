#+title: Minutes of Meeting  
#+AUTHOR: VLEAD
#+DATE: [2018-07-12 Thu]
#+SETUPFILE: ../../org-templates/level-2.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
 Here are the Minutes of meeting had so far for our project:

|--------+--------------------------------------------------+------------|
| *SNo.* | *Purpose*                                        | *Link*     |
|--------+--------------------------------------------------+------------|
|     1. | To Review experiment structure and story board   | [[./2018-05-18-review-mom.org][MOM-1 link]] |
|--------+--------------------------------------------------+------------|
|     2. | Meeting with Prof. Choppella and Mr. Mrithyunjay | [[./2018-05-25-common-mom.org][MOM-2 link]] |
|--------+--------------------------------------------------+------------|
|     3. | Meeting with UI/UX Team                          | [[./2018-05-30-ui-ux-mom.org][MOM-3 link]] |
|--------+--------------------------------------------------+------------|
