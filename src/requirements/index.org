#+Title: Project Requirements(Linked lists: single and doubly linked lists) 
#+Date: [2018-05-17 Thu]
#+PROPERTY: results output
#+PROPERTY: exports code
#+options: ^:nil

* Guidelines to follow: 
1. When introducing a concept, do it in 4 steps: 
   + Define
   + Show (given an example)
   + Practice (let them try it out)
   + Quiz (for reiteration of key concepts)

2. Think in terms of a generic structure: 
   + Pre-requisites/Pre-read
   + Learning Objectives
   + Pre-test
   + Introduction
   + <One or more sub-modules>
   + Post-test
   + Key takeaways
   + Additional resources

3. Think of new concepts that need to be introduced and introduce them
   separately. For example, when talking about Mergesort, split, merge
   are important new concepts.

Gagne’s Nine Events of Instruction

    Gain attention
    Inform learners of objectives
    Stimulate recall of prior learning
    Present the content
    Provide “learning guidance”
    Elicit performance (practice)
    Provide feedback
    Assess performance
    Enhance retention and transfer to the job
 
* Topics to cover

Linked Lists: Singly linked lists: Representation in
memory, Algorithms of several operations: Traversing, Searching,
Insertion into, Deletion from linked list; Linked representation of
Stack and Queue, Header nodes, Doubly linked list: operations on it
and algorithmic analysis; Circular Linked Lists: all operations their
algorithms and the complexity analysis.

** What to focus on
1. What is a linked list? - Data structure for holding a list of items
   (numbers, strings, objects)
2. An item has a location reference, and a value stored at the
   location
3. List has a few primitive operations and associated time complexity
   of doing the operation
   + Find() 
   + Insert()
   + Delete()
4. How to perform these primitive operations on singly and doubly
   linked lists
5. Implementing specialized structures (stacks, queues) using this
   data structure

https://en.wikipedia.org/wiki/Linked_list

